﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cassini.MBTA.BostonTransit
{
	public class App : Application
	{
		ListView listView = new ListView
		{
			HasUnevenRows = true
		};
		List<MbtaRoute> routes = new List<MbtaRoute> ();

		public App ()
		{
			listView.ItemsSource = routes;
			listView.ItemTemplate = new DataTemplate (typeof(MbtaRouteCell));

			// The root page of your application
			MainPage = new ContentPage {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.FillAndExpand,
					Children = {
						listView
					}
				}
			};
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
			Task.Factory.StartNew (async () => {
				var predictions = await (new RoutesMonitor ()).FetchPredictions ();
				Device.BeginInvokeOnMainThread (delegate {
					routes.Clear ();
					routes.AddRange (predictions);

					listView.ItemsSource = null;
					listView.ItemsSource = routes;
				});
			});
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

