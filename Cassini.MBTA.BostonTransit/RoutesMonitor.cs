﻿using System;
using System.Threading.Tasks;
using Cassini.MBTA.Core;
using System.Linq;
using System.Collections.Generic;

namespace Cassini.MBTA.BostonTransit
{
	public class RoutesMonitor
	{
		float latitude = 42.369504f;
		float longitude = -71.039601f;

		public async Task<List<MbtaRoute>> FetchPredictions ()
		{
			Dictionary <string, MbtaRoute> routeToStop = new Dictionary<string, MbtaRoute> ();
			var stops = await Stops.ByLocationAsync (latitude, longitude);
			foreach (var stop in stops.Take (5)) {
				var predictions = await Predictions.ByStopAsync (stop.Id);
				foreach (var routeMode in predictions.Modes) {
					foreach (var route in routeMode.Routes) {
						if (!routeToStop.ContainsKey (route.Name)) {
							routeToStop.Add (route.Name, new MbtaRoute {
								RouteName = route.Name.Split (new [] {' '}).First ().ToUpper (),
								StopName = stop.StopName
							});
							System.Diagnostics.Debug.WriteLine(route.Name +":\t"+stop.StopName);
						}
					}
				}
			}

			return routeToStop.Values.ToList ();
		}
	}
}

