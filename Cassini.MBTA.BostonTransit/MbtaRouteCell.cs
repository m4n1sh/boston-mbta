﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace Cassini.MBTA.BostonTransit
{
	public class MbtaRouteCell : ViewCell
	{
		public MbtaRouteCell ()
		{
			var viewLayout = new StackLayout()
			{
				Orientation = StackOrientation.Vertical,
				Padding = new Thickness (6)
			};
			foreach (var view in GetRouteView ())
				viewLayout.Children.Add (view);
			View = viewLayout;
		}

		IEnumerable<View> GetRouteView ()
		{
			var routeNameLabel = new Label
			{
				HorizontalOptions= LayoutOptions.Start,
				FontSize = 36,
				FontAttributes = FontAttributes.Bold,

			};
			routeNameLabel.SetBinding(Label.TextProperty, "RouteName");

			var stopNameLabel = new Label
			{
				HorizontalOptions= LayoutOptions.EndAndExpand,
				LineBreakMode = LineBreakMode.WordWrap,
				WidthRequest = 200,
				XAlign = TextAlignment.End
			};
			stopNameLabel.SetBinding(Label.TextProperty, "StopName");

			var routeHeader = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness (3),
				Children = { routeNameLabel, stopNameLabel }
			};


			yield return routeHeader;

		}
	}
}

