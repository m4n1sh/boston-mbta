﻿using System;

namespace Cassini.MBTA.BostonTransit
{
	public class MbtaRoute
	{
		public string RouteName { get; set; }

		public string StopName { get; set; }
	}
}

